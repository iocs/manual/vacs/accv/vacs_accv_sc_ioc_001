# Startup for VacS-ACCV:SC-IOC-001

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(IOCSH_TOP)/iocsh/vacs_accv_vac_plc_01001.iocsh", "DBDIR=$(IOCSH_TOP)/db/, MODVERSION=$(IOCVERSION=)")

