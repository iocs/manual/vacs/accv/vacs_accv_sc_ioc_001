########################### Vacuum Angle/Gate Valve Control ############################


#-############################
#- COMMAND BLOCK
#-############################

define_command_block()

add_digital("AutoCmd",                         PV_NAME="AutoModeCmd",                   PV_DESC="Set control mode to AUTOMATIC")
add_digital("ManuCmd",                         PV_NAME="ManualModeCmd",                 PV_DESC="Set control mode to MANUAL")
add_digital("CloseManuCmd",                    PV_NAME="ManualCloseCmd",                PV_DESC="Manual CLOSE command")
add_digital("OpenManuCmd",                     PV_NAME="ManualOpenCmd",                 PV_DESC="Manual OPEN command")
add_digital("BypassItlCmd",                    PV_NAME="OverrideITLckCmd",              PV_DESC="Override tripped interlock")
add_digital("BlockClosedCmd",                  PV_NAME="AutoOpenDisCmd",                PV_DESC="Disable automatic OPEN")
add_digital("ResetCmd",                        PV_NAME="RstErrorsCmd",                  PV_DESC="Reset errors and warnings")
#-----------------------------
add_digital("EPICSItlReq",                     PV_NAME="ITLck_EPICS_TrigCmd",           PV_DESC="Trigger EPICS interlock")
add_digital("ClearBypassCmd",                  PV_NAME="ClrOverrideITLckCmd",           PV_DESC="Clear interlock override")
add_digital("UnlockCmd",                       PV_NAME="AutoOpenEnaCmd",                PV_DESC="Enable automatic OPEN")
add_digital("ClearCountersCmd",                PV_NAME="ClrCountersCmd",                PV_DESC="Clear counters")


#-############################
#- STATUS BLOCK
#-############################

define_status_block()

add_major_alarm("ErrorSt",	"Tripped",         PV_NAME="ErrorR",                        PV_DESC="Error detected by the control function",  PV_ONAM="Error", ARCHIVE=True)
#-----------------------------
add_digital("ManuSt",                          PV_NAME="ManualModeR",                   PV_DESC="Control mode status",                     PV_ONAM="Manual",               PV_ZNAM="Auto")
add_digital("CloseSt",                         PV_NAME="ClosedR",                       PV_DESC="The valve is CLOSED",                     PV_ONAM="Closed", ARCHIVE=True)
add_digital("UndefinedSt",                     PV_NAME="UndefinedR",                    PV_DESC="The valve is neither OPEN nor CLOSED",    PV_ONAM="Undefined")
add_digital("OpenSt",                          PV_NAME="OpenR",                         PV_DESC="The valve is OPEN",                       PV_ONAM="Open", ARCHIVE=True)
add_digital("InterlockTriggerSt",			   PV_NAME="ITLckTrigR",                    PV_DESC="Interlock triggering status",             PV_ONAM="NominalState")
#-----------------------------
add_digital("OpenDQSt",                        PV_NAME="OpenDQ-RB",                     PV_DESC="Status of the OPEN digital output",       PV_ONAM="True",                 PV_ZNAM="False")
add_digital("OpenManuSt",                      PV_NAME="OnManualOpenR",                 PV_DESC="Current OPEN command was MANUAL",         PV_ONAM="Manual Open")
add_digital("OpenAutoSt",                      PV_NAME="OnAutoOpenR",                   PV_DESC="Current OPEN command was AUTOMATIC",      PV_ONAM="Automatic Open")
add_digital("LockedSt",                        PV_NAME="AutoOpenDisStR",                PV_DESC="Automatic OPEN toggle status",            PV_ONAM="Auto open Disabled",   PV_ZNAM="Auto open Enabled")
add_digital("WarningSt",                       PV_NAME="WarningR",                      PV_DESC="A warning is active",                     PV_ONAM="Warning")
add_digital("InvalidCommandSt",                PV_NAME="InvalidCommandR",               PV_DESC="Last command sent cannot be processed",   PV_ONAM="Invalid command")
add_digital("ValidSt",                         PV_NAME="ValidR",                        PV_DESC="Communication is valid",                  PV_ONAM="Valid",                PV_ZNAM="Invalid")

add_digital("HardWItlNo1HealthySt",            PV_NAME="ITLck_HW_1_HltyR",              PV_DESC="Hardware:1 interlock is HEALTHY",         PV_ONAM="HEALTHY")
add_digital("HardwareInterlockNo1St",          PV_NAME="ITLck_HW_1_TrpR",   		 	PV_DESC="Hardware:1 interlock is TRIPPED")
add_digital("HardWItlNo1BypassSt",             PV_NAME="ITLck_HW_1_OvRidnR",            PV_DESC="Hardware:1 interlock is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable Hardware Interlock No1",  PV_NAME="ITLck_HW_1_DisR",               PV_DESC="Hardware:1 interlock is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
add_digital("PressItlNo1HealthySt",            PV_NAME="ITLck_Prs_1_HltyR",             PV_DESC="Pressure:1 interlock is HEALTHY",         PV_ONAM="HEALTHY")
add_digital("PressureInterlockNo1St",          PV_NAME="ITLck_Prs_1_TrpR",  			PV_DESC="Pressure:1 interlock is TRIPPED")
add_digital("PressItlNo1BypassSt",             PV_NAME="ITLck_Prs_1_OvRidnR",           PV_DESC="Pressure:1 interlock is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable Pressure Interlock No1",  PV_NAME="ITLck_Prs_1_DisR",              PV_DESC="Pressure:1 interlock is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
#-----------------------------
add_digital("HardWItlNo2HealhtySt",            PV_NAME="ITLck_HW_2_HltyR",              PV_DESC="Hardware:2 interlock is HEALTHY",         PV_ONAM="HEALTHY")
add_digital("HardwareInterlockNo2St",          PV_NAME="ITLck_HW_2_TrpR",   			PV_DESC="Hardware:2 interlock is TRIPPED")
add_digital("HardWItlNo2BypassSt",             PV_NAME="ITLck_HW_2_OvRidnR",            PV_DESC="Hardware:2 interlock is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable Hardware Interlock No2",  PV_NAME="ITLck_HW_2_DisR",               PV_DESC="Hardware:2 interlock is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
add_digital("PressItlNo2HealthySt",            PV_NAME="ITLck_Prs_2_HltyR",             PV_DESC="Pressure:2 interlock is HEALTHY",         PV_ONAM="HEALTHY")
add_major_alarm("PressureInterlockNo2St",  "TRIPPED",      PV_NAME="ITLck_Prs_2_TrpR",  PV_DESC="Pressure:2 interlock is TRIPPED")
add_digital("PressItlNo2BypassSt",             PV_NAME="ITLck_Prs_2_OvRidnR",           PV_DESC="Pressure:2 interlock is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable Pressure Interlock No2",  PV_NAME="ITLck_Prs_2_DisR",              PV_DESC="Pressure:2 interlock is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")

add_digital("HardWItlNo3HealthySt",            PV_NAME="ITLck_HW_3_HltyR",              PV_DESC="Hardware:3 interlock is HEALTHY",         PV_ONAM="HEALTHY")
add_digital("HardwareInterlockNo3St",          PV_NAME="ITLck_HW_3_TrpR",   			PV_DESC="Hardware:3 interlock is TRIPPED")
add_digital("HardWItlNo3BypassSt",             PV_NAME="ITLck_HW_3_OvRidnR",            PV_DESC="Hardware:3 interlock is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable Hardware Interlock No3",  PV_NAME="ITLck_HW_3_DisR",               PV_DESC="Hardware:3 interlock is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
add_digital("SoftWItlHealthySt",               PV_NAME="ITLck_SW_HltyR",                PV_DESC="Software interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_digital("SoftwareInterlockSt",             PV_NAME="ITLck_SW_TrpR",     			PV_DESC="Software interlock is TRIPPED")
add_digital("SoftWItlBypassSt",                PV_NAME="ITLck_SW_OvRidnR",              PV_DESC="Software interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Software Interlock",      PV_NAME="ITLck_SW_DisR",                 PV_DESC="Software interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
#-----------------------------
add_digital("OpenItlHealthySt",                PV_NAME="ITLck_Open_HltyR",              PV_DESC="Open interlock is HEALTHY",               PV_ONAM="HEALTHY")
add_digital("OpenInterlockSt",  			   PV_NAME="ITLck_Open_TrpR",               PV_DESC="Open interlock is TRIPPED")
add_digital("OpenItlBypassSt",                 PV_NAME="ITLck_Open_OvRidnR",            PV_DESC="Open interlock is OVERRIDEN",             PV_ONAM="OVERRIDEN")
add_digital("Disable Open Interlock",          PV_NAME="ITLck_Open_DisR",               PV_DESC="Open interlock is NOT CONFIGURED",        PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
add_digital("EPICSItlHealthySt",               PV_NAME="ITLck_EPICS_HltyR",             PV_DESC="Interlock from EPICS is HEALTHY",         PV_ONAM="HEALTHY")
add_digital("EPICSInterlockSt", 			   PV_NAME="ITLck_EPICS_TrpR",              PV_DESC="Interlock from EPICS is TRIPPED")
add_digital("EPICSItlBypassSt",                PV_NAME="ITLck_EPICS_OvRidnR",           PV_DESC="Interlock from EPICS is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable EPICS Interlock",         PV_NAME="ITLck_EPICS_DisR",              PV_DESC="Interlock from EPICS is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")

add_analog("WarningCode",          "BYTE",     PV_NAME="WarningCodeR",                  PV_DESC="Active warning code")

add_analog("ErrorCode",            "BYTE",     PV_NAME="ErrorCodeR",                    PV_DESC="Active error code", ARCHIVE=True)

add_analog("RunTime",              "REAL",     PV_NAME="RunTimeR",                      PV_DESC="Runtime in hours", PV_EGU="h", ARCHIVE=True)

add_analog("OpenCounter",          "UDINT",    PV_NAME="OpenCounterR",                  PV_DESC="Number of opens", ARCHIVE=True)

add_verbatim("""
record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_1_Device")
{
	field(DESC, "Device assigned to this interlock")
	field(VAL,  "[PLCF#ext.dash_means_empty("VVAG_ITLck_HW_1")]")
	field(PINI, "YES")
}

record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_2_Device")
{
	field(DESC, "Device assigned to this interlock")
	field(VAL,  "[PLCF#ext.dash_means_empty("VVAG_ITLck_HW_2")]")
	field(PINI, "YES")
}

record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_3_Device")
{
	field(DESC, "Device assigned to this interlock")
	field(VAL,  "[PLCF#ext.dash_means_empty("VVAG_ITLck_HW_3")]")
	field(PINI, "YES")
}

record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_1_Device")
{
	field(DESC, "Device assigned to this interlock")
	field(VAL,  "[PLCF#ext.dash_means_empty("VVAG_ITLck_Prs_1")]")
	field(PINI, "YES")
}

record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_2_Device")
{
	field(DESC, "Device assigned to this interlock")
	field(VAL,  "[PLCF#ext.dash_means_empty("VVAG_ITLck_Prs_2")]")
	field(PINI, "YES")
}

record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_Device")
{
	field(DESC, "Device assigned to this interlock")
	field(VAL,  "[PLCF#ext.dash_means_empty("VVAG_ITLck_Open")]")
	field(PINI, "YES")
}
""")

add_verbatim("""
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_1_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_1_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_1_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_1_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_1_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_1_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_2_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_2_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_2_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_2_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_2_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_2_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_3_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_3_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_3_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_Open_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_EPICS_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_EPICS_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_EPICS_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_1_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_1_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_1_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_1_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_1_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_1_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_2_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_2_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_2_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_2_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_2_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_2_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_3_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_3_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_3_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_Open_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_EPICS_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_EPICS_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_EPICS_DisR CP")
	field(CALC, "A || B")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "Interlocks are HEALTHY")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_1_HltyR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_1_HltyR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_2_HltyR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_2_HltyR CP")
	field(INPF, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_3_HltyR CP")
	field(INPG, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_HltyR CP")
	field(INPH, "[PLCF#INSTALLATION_SLOT]:#ITLck_Open_HltyR CP")
	field(INPI, "[PLCF#INSTALLATION_SLOT]:#ITLck_EPICS_HltyR CP")

	field(CALC, "!A && B && C && D && E && F && G && H && I")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_HltyR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_HltyR")
{
	field(DESC, "Interlocks are HEALTHY")
	field(ONAM, "HEALTHY")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "At least one interlock is TRIPPED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_1_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_1_TrpR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_2_TrpR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:#ITLck_Prs_2_TrpR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_3_TrpR CP")
	field(INPF, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_TrpR CP")
	field(INPG, "[PLCF#INSTALLATION_SLOT]:#ITLck_Open_TrpR CP")
	field(INPH, "[PLCF#INSTALLATION_SLOT]:#ITLck_EPICS_TrpR CP")

	field(CALC, "A || B || C || D || E || F || G || H")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR")
{
	field(DESC, "At least one interlock is TRIPPED")
	field(OSV,  "MAJOR")
	field(ONAM, "TRIPPED")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_OvRidnR")
{
field(SCAN, "1 second")
	field(DESC, "At least one interlock is OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_1_OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_1_OvRidnR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_2_OvRidnR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_2_OvRidnR CP")
	field(INPF, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_3_OvRidnR CP")
	field(INPG, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_OvRidnR CP")
	field(INPH, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_OvRidnR CP")
	field(INPI, "[PLCF#INSTALLATION_SLOT]:ITLck_EPICS_OvRidnR CP")

	field(CALC, "!A && (B || C || D || E || F || G || H || I)")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_OvRidnR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_OvRidnR")
{
	field(DESC, "At least one interlock is OVERRIDEN")
	field(ONAM, "OVERRIDEN")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_DisR")
{
field(SCAN, "1 second")
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_1_DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_1_DisR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_2_DisR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_Prs_2_DisR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_3_DisR CP")
	field(INPF, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_DisR CP")
	field(INPG, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_DisR CP")
	field(INPH, "[PLCF#INSTALLATION_SLOT]:ITLck_EPICS_DisR CP")

	field(CALC, "A && B && C && D && E && F && G && H")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_DisR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_DisR")
{
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(ONAM, "NOT CONFIGURED")
	field(ZNAM, "CONFIGURED")
	field(DISP, "1")
}


record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLckStatR")
{
field(SCAN, "1 second")
	field(DESC, "Calculate combined interlock status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_HltyR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_DisR CP")

# Check that only one is true
	field(CALC, "E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
	field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStatR PP")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStatR")
{
	field(DESC, "Combined Interlock status")
	field(ZRST, "INVALID")
	field(ONST, "HEALTHY")
	field(TWST, "TRIPPED")
	field(TWSV, "MAJOR")
	field(THST, "OVERRIDEN")
	field(FRST, "NOT CONFIGURED")
}
""")

add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:#StatR")
{
field(SCAN, "1 second")
	field(DESC, "Calculate valve status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:UndefinedR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:OpenR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ClosedR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ErrorR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:ValidR CP MSS")

# Recalculate 'undefined' (incorrect in PLC)
	field(CALC, "A:=(B==C);F:=(A || B || C || D);F")
	field(OCAL, "E&&F?(D?4:(A?1:B*2+C*3)):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:StatR PP MSS")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:StatR")
{
	field(DESC, "Valve status")
	field(ZRST, "INVALID")
	field(ONST, "UNDEFINED")
	field(TWST, "OPEN")
	field(THST, "CLOSED")
	field(FRST, "ERROR")
	field(FRSV, "MAJOR")
}
""")


#-############################
#- METADATA
#-############################
#- ERRORS
define_metadata("error", MD_SCRIPT='css_code2msg.py', CODE_TYPE='error')
#-
add_metadata(99, 'Open Status & Closed Status Both Active')
add_metadata(98, 'Valve Not Closed')
add_metadata(97, 'Pressure Interlock No. 1')
add_metadata(96, 'Hardware Interlock No. 1')
add_metadata(95, 'Pressure Interlock No. 2')
add_metadata(94, 'Hardware Interlock No. 2')
add_metadata(93, 'Hardware Interlock No. 3')
add_metadata(92, 'Software Interlock (From "External" PLC Function)')
add_metadata(49, 'Pressure Interlock No. 1 - Auto Reset')
add_metadata(48, 'Hardware Interlock No. 1 - Auto Reset')
add_metadata(47, 'Pressure Interlock No. 2 - Auto Reset')
add_metadata(46, 'Hardware Interlock No. 2 - Auto Reset')
add_metadata(45, 'Hardware Interlock No. 3 - Auto Reset')
add_metadata(44, 'Software Interlock - Auto Reset')
#-
#- WARNINGS
define_metadata("warning", MD_SCRIPT='css_code2msg.py', CODE_TYPE='warning')
#-
add_metadata(99, 'Valve Not Open')
add_metadata(98, 'Bypass Interlock Time-Out')
add_metadata(97, 'Open Interlock Active (Valve Closed)')
add_metadata(96, 'Valve Opening Prevented by Tripped Interlock')
add_metadata(15, 'Open Interlock Active (Valve Open)')
add_metadata(14, 'Pressure Interlock No. 1 Bypassed / Overridden')
add_metadata(13, 'Hardware Interlock No. 1 Bypassed / Overridden')
add_metadata(12, 'Pressure Interlock No. 2 Bypassed / Overridden')
add_metadata(11, 'Hardware Interlock No. 2 Bypassed / Overridden')
add_metadata(10, 'Hardware Interlock No. 3 Bypassed / Overridden')
add_metadata( 9, 'Software Interlock Bypassed / Overridden')
add_metadata( 8, 'Open Interlock Active (Valve Closed)')
add_metadata( 7, 'Valve Opening Prevented by Tripped Interlock')
